---
layout: post
title: "머신러닝을 통한 로또 당첨 번호 예측(랜덤포레스트회귀 모델 feat.chat..."
toc: true
---

 사용한 모델은 랜덤포레스트회귀 모델로 습업 데이터의 특성(feature)과 정답(target) 간의 관계를 학습하여, 새로운 입력 데이터가 들어왔을 단시 정답을 예측하는 모델로 이를 이용해 전차 로또 번호의 일부를 수습 데이터로 사용하고, 최후 로또 번호의 일부를 정답(target)으로 지정하여 모델을 학습시킨다.
 이를 반복하여 극점 5개의 데이터를 사용하여 6개의 당첨 번호를 예측하게 되는 모델이다.

 

 로또 번호는 랜덤성이 세상없이 정도 그렇게 누구 머신러닝 [로또 번호](https://wrong-crib.com/life/post-00042.html) 모델을 사용하더라도 정확도를 장담하기는 어렵다.
로또 번호 예측에 대한 기대는 갖지 말자

 

 앞서 경관 결과부터
 

 

 안나올 것 같은 구성인데.....

 #필요한 package import
 from sklearn.ensemble import RandomForestRegressor
 sklearn.ensemble: scikit-learn 패키지의 앙상블 거울 결부 모듈로, RandomForestRegressor 등 앙상블 모델을 제공
 from sklearn.multioutput import MultiOutputRegressor
 sklearn.multioutput: scikit-learn 패키지의 민중 출력 모범 결부 모듈로, MultiOutputRegressor 등 다중 출력 모델을 제공
 from sklearn.model_selection import train_test_split
klearn.model_selection: scikit-learn 패키지의 교범 선택 및 평가와 관련된 모듈로, train_test_split 등 데이터셋을 분리하고 교차 검증을 수행하는 함수를 제공
 

 # data 불러오기
 lotto_data = np.load("/content/drive/MyDrive/Colab Notebooks/lotto1057.npy")
 데이터는 기존에 작성한 코드를 사용하여 동행복권에서 1~1057회차 역대 당첨번호를 크롤링한 후 numpy로 저장해두었고
 이를 불러와서 사용하였다. 이는 다리 회차에서 추첨된 번호들을 총체 합친 인명부 형태로 구성
해당 코드는 맨 아래 첨부
 

 # 학습 데이터 준비
n_samples = len(lotto_data) - 11
 전통 데이터에서 -11(X, y 값으로 가늠 할 번호를 구하기 위해 적용 할 값을 제외하고 샘플데이터로 만든다.
 X = np.zeros((n_samples, 5))
 X는 n_samples개의 로또 번호 데이터에서 제각각 앞에서부터 5개의 번호를 추출하여 저장하는 2차원 배열로 각각의 데이터는 당첨번호 안 5개가 들어있다.
 y = np.zeros((n_samples, 6))
 y 변수는 n_samples개의 로또 번호 데이터에서 번번이 뒤에서부터 6개의 번호를 추출하여 정렬하여 저장하는 2차원 배열로 X의 뒤에 나온 정수 6개가 저장된다.
 for i in range(n_samples):
    X[i] = lotto_data[i:i+5]
    y[i] = np.sort(lotto_data[i+5:i+11])
 반복하여 X,y값 저장
 

 예를들어 1회차 당첨번호가 [1, 2, 3, 4, 5, 6 ] 2회차 당첨번호가 [7, 8, 9, 10, 11, 12] 로 구성되어있으면?
 첫 번째 학습 데이터: [1, 2, 3, 4, 5] / [6, 7, 8, 9, 10, 11]
 두 번째 학업 데이터: [2, 3, 4, 5, 6] / [7, 8, 9, 10, 11, 12] 로 만들어지게 된다.

 첫 번째 연마 데이터에서 입력 변수 X는 1, 2, 3, 4, 5이고 출력 변수 y는 6, 7, 8, 9, 10, 11가 된다.
 

 # 학습 데이터와 검증 데이터로 분리
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
 rain_test_split 함수는 데이터셋을 습련 데이터와 검증 데이터로 나눌 사이 사용하는 함수로 이를 사용하여 데이터셋을 무작위로 섞은 후, 분리 비율에 따라 수습 데이터와 검증 데이터로 나눌 운 있다.
 X는 입력 데이터, y는 출력 데이터, test_size는 검증 데이터의 비율을 의미하고 데이터셋의 20%를 검증 데이터로 사용하도록 설정되어있다. random_state는 난수 시드값
 

 # MultiOutputRegressor 모델 생성
model = MultiOutputRegressor(RandomForestRegressor(n_estimators=150, random_state=42))
 위 코드는 Scikit-learn의 RandomForestRegressor 모델을 사용하여 민서 출력 회귀(하나의 인풋 값에 대해 여러 전민 출력 값을 예측하는) 문제를 해결하기 위해 MultiOutputRegressor 모델을 생성하는 코드

 

 RandomForestRegressor 모델은 앙상블 학습(Ensemble Learning)의 일종으로, 여러 노신 지각 성적 트리(Decision Tree)를 조합하여 예측. n_estimators 인자를 통해 사용할 배려 낙착 트리의 개수를 지정
 random_state 인자는 난수 발생 시드(Seed)를 지정하는 값으로, 난수 구축 시드가 같으면 습업 결과가 동일하다.

 같은 난수 조성 시드를 사용하면 동일한 결과가 나온다. random_state 값이 같다면 누가 하던 같은 결과를 볼수 있다. 

 

 # 모델 학습
model.fit(X_train, y_train)
위 코드는 생성한 모델을 학습시키는 부분으로 X_train은 연마 데이터의 입력값, y_train은 수업 데이터의 출력값이 되며,  모델은 fit 함수를 통해 이익금 두 값에 대해 학습하게 된다.

 

 # 검증 데이터로 예측
y_pred = model.predict(X_test)
위 코드는 모델이 학습된 후, 검증 데이터인 X_test를 모델에 입력하여 예측값을 생성
model.predict(X_test) 코드를 실행하면 모델은 X_test를 입력으로 받아서 다리 입력에 대한 출력값을 예측하고, 수지 출력값들을 numpy 배치 형태로 반환한다. 이식 때, y_pred 변수에는 모델이 예측한 출력값들이 저장
 

 # 검증 결과 평가
score = model.score(X_test, y_test)
 print("R² score:", score)
예측값들과 y_test에 저장된 실제값들을 비교하여 모델의 성능을 평가하는  R² 값을 구한다.

 R² score는 회귀 모델의 가늠 성능을 평가하는 기준 중간 하나로 0에서 1 사이의 값을 가지며, 1에 가까울수록 모델의 예측이 정확하다는 것을 의미. 음수가 나온다면 못쓰는 모델

# 다음 회차 로또 번호 예측
next_lotto_numbers = model.predict([lotto_data[-5:]])[0]
next_lotto_numbers = np.round(next_lotto_numbers).astype(int)
next_lotto_numbers = sorted(next_lotto_numbers)[-6:]
print("다음 로또 번호 예측 결과:", next_lotto_numbers)
 

 양반 최근의 5개 로또 번호를 입력값으로 하여, 모델을 이용하여 예측한 6개의 숫자를 저장(정수형으로 안나옴)
 np.round(next_lotto_numbers).astype(int): 예측된 숫자를 반올림한 차기 정수형으로 변환한다.
 아울러 정렬 추후 최종적으로 예측된 6개의 로또 번호를 출력
 

 전체코드
 

 

 아래는 3.4일 1057회차 까지의 당첨결과를 크롤링해서 numpy 형식으로 저장하고 당첨번호 스타 등장횟를 체크하는 코드로
 지난 문헌 참조
 과실 코드는 requests 모듈을 사용하여 동행복권 url에 접속해서 추첨 결과를 가져오고, json 형식으로 파싱한다.
 아울러 매개 당첨번호들을 num1, num2, num3, num4, num5, num6 리스트에 추가한다.
 네놈 다음으로, numpy 모듈을 사용하여 h 리스트를 생성하고 하지 회차에서 추첨된 번호들을 총체 합친다. 그리고, collections 모듈의 Counter 함수를 사용하여 h 리스트에서 과정 숫자가 몇 체차 등장했는지 카운트한 결과를 출력.
 이를 통해 아래가지 번호별로 여북 수다히 나왔는지 확인이 가능하다.
 천행 결과는 아래와 같으며 마지막으로, numpy 모듈을 사용하여 추첨번호들을 저장한다.

 

 

 1등되면 착하게 살께요 한번만 좀
